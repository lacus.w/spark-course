//载入硬盘上的测试文件，一共是1073行
val lines = sc.textFile("file:///home/hadoop/exp13/task1/chapter5-data1.txt")

//1，求姓名的数量:265
lines.map(_.split(",")(0)).distinct.map( a => (a,"")).sortByKey().count

//2，求课程的数量
lines.map(_.split(",")(1)).distinct.map( a => (a,"")).sortByKey().collect

//3，某个学生的总分和均值
val allScore = lines.filter(a => a.contains("Tom")).map(_.split(",")(2)) //得到所有的成绩

var num = 0 //num变量用于计算个数
var sum = 0 //保存所有成绩的总和
allScore.map(a => a.toInt).collect.foreach( x => { 
    num = num + 1
    sum = sum + x
}) //用法参考课本132页代码
sum // 五门课的总和
sum/num //均值

//4. 每名同学选修的课程门数；
//得到（姓名，科目名）的成对数据
val pairs = lines.map(_.split(",")).map(a=>(a(0),a(1)))
// 查看前十条记录：
pairs.take(10)
// 再映射一次，每门课都算一次，所以每个名字后面都填1
val p1 = pairs.mapValues(a => 1) 
// 查看前十条记录：
p1.take(10)
//分组统计个数，组内两两相加
val p2 = p1.reduceByKey((a,b) => a + b)
//分行打印在屏幕上，一共是265对数据
p2.foreach(println)
p2.count


//（5）该系 DataBase 课程共有多少人选修；

// 先选出包含DataBase的所有行：
val database_data = lines.filter(_.contains("DataBase"))
// 得到姓名这一列：
val names = database_data.map(_.split(",")(0))
//所有姓名去重后，得到个数，结果是125
names.distinct.count


//（6）各门课程的平均分是多少；

//得到（科目名，成绩）的成对数据
val pairs = lines.map(_.split(",")).map(a=>(a(1),a(2)))
// 可以先查看一下前十对:
pairs.take(10)
// 再映射一次，科目名不变，成绩后面跟上1，之后用于累加，所以是键值对里面嵌套了一层键值对
val p1 = pairs.mapValues(a => (a.toInt, 1))
//分组聚合，组内加成绩的同时也把个数相加，也就是（成绩，1）这两个栏目都在逐行累计求和
val p2 = p1.reduceByKey((a,b) => (a._1+b._1, a._2+b._2))
// 查看求和后的前十对数据：
p2.take(10)
// 累加无误的话就可以除法运算得到均值
val p3 = p2.mapValues(a => a._1/a._2)
p3.collect //p3就是最终结果了
// 分行打印可以使用foreach：
p3.foreach(println)



//（7）使用累加器计算共有多少人选修了 DataBase 课程。
// 映射成键值对：
val pairs = lines.map(a => a.split(",")).map(a => (a(1),a(0)))
// 分组，在组内组合，参考课本123页代码
val result = pairs.combineByKey(
    (name) => (1),
    (acc:Int, name) => (acc+1),
    (a:Int, b:Int) => (a+b)
)

// 查看组合结果，里面是所有课的选课人数：
result.collect

//单看某门课
result.filter(_._1.contains("DataBase")).collect