/*

对于两个输入文件 A 和 B，编写 Spark 独立应用程序，对两个文件进行合并，并剔除其中重复的内容，得到一个新文件C。下面是输入文件和输出文件的样例，供参考。
输入文件A的样例如下。
20170101 x
20170102 y
20170103 x
20170104 y
20170105 z
20170106 z
输入文件B的样例如下。
20170101 y
20170102 y
20170103 x
20170104 z
20170105 y
将输入文件A和B合并得到的输出文件C的样例如下。
(20170101,x)
(20170101,y)
(20170102,y)
(20170103,x)
(20170104,y)
(20170104,z)
(20170105,z)
(20170105,y)
(20170106,z)
*/

// 载入本地文件
val file_a = sc.textFile("file:///home/hadoop/exp13/task2/file_a")
val file_b = sc.textFile("file:///home/hadoop/exp13/task2/file_b")

// 两部分拼接在一起，也就是并集
val lines = file_a.union(file_b)

// 查看A∪B的结果:
lines.collect
// 分行打印：一共11行
lines.foreach(println)

// 排序一下再打印：这里要参考课本133页分区代码，比较难可以略过
import org.apache.spark.HashPartitioner
lines.map(a => a.split(" ")).map(a => (a(0).toInt,a(1))).partitionBy(new HashPartitioner(1)).sortByKey().foreach(println)

// 重复的行去掉，剩下9行
val result = lines.distinct
// 再排序打印
result.map(a => a.split(" ")).map(a => (a(0).toInt,a(1))).partitionBy(new HashPartitioner(1)).sortByKey().foreach(println)

// 输出到文件C
result.saveAsTextFile("file:///home/hadoop/exp13/task2/result")