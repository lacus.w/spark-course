/*

每个输入文件表示班级学生某个学科的成绩，每行内容由两个字段组成，第一个是学生名字，第二个是学生成绩。编写Spark独立应用程序求出每名学生的平均成绩，并输出到一个新文件中。下面是输入文件和输出文件的样例，供参考。
Algorithm成绩如下。
小明 92
小红 87
小新 82
小丽 90
Database成绩如下。
小明 95
小红 81
小新 89
小丽 85
Python成绩如下。
小明 82
小红 83
小新 94
小丽 91

*/

// 载入数据
val fa = sc.textFile("file:///home/hadoop/exp13/task3/Algorithm")
val fd = sc.textFile("file:///home/hadoop/exp13/task3/Database")
val fp = sc.textFile("file:///home/hadoop/exp13/task3/Python")

// 处理成三组键值对数据
val pair_a = fa.map(_.split(" ")).map(a => (a(0),a(1).toInt))
val pair_d = fd.map(_.split(" ")).map(a => (a(0),a(1).toInt))
val pair_p = fp.map(_.split(" ")).map(a => (a(0),a(1).toInt))

// 内连接三个键值对，具体用法参考课本122-123页
val p1 = pair_a.join(pair_d).join(pair_p)

// 查看连接结果，这个时候键值对里面嵌套了两层
p1.take(2)

// 映射，得到（张三，总和）的形式，三门成绩一共加了两次
val p2 = p1.mapValues(a => a._1._1 + a._1._2 + a._2)
// 查看求和后的结果
p2.collect

// 根据求和的结果算均分，总和除以三就可以了
val p3 = p2.mapValues(a => a*1.0/3)
// 要保留2位小数的话就再映射一次
val p4 = p3.mapValues(a => "%.2f".format(a))
// 分行打印
p4.foreach(println)

// 结果无误后输出到文件
p4.map(a => "(" + a._1 +","+a._2.toString + ")").saveAsTextFile("file:///home/hadoop/exp13/task3/result")
 