// parquet数据源的存储路径
val filePath = "file:///home/hadoop/hadoop/spark-3.2.3/examples/src/main/resources/users.parquet"

// 读parquet数据源到DateFrame
val df = spark.read.parquet(filePath)
// 第二种写法是 val df = spark.read.format("parquet").load(filePath)，看起来复杂一点点

// 查看df
df.show

//查看表结构
df.printSchema

// df写入到本地某个目录中
df.write.parquet("file:///home/hadoop/exp14/otherusers2")
// 或者写成 df.write.format("parquet").mode("overwrite").option("compression","snappy").save("file:///home/hadoop/exp14/task1") 


/*********************************/

val filePath = "file:///home/hadoop/hadoop/spark-3.2.3/examples/src/main/resources/people.json"

// 读json数据源到DateFrame
val df = spark.read.json(filePath)
// 查看df
df.show
df.printSchema

// 将表数据保存为json格式的文件
df.write.json("file:///home/hadoop/exp14/task2")


/*********************************/


val filePath = "file:///home/hadoop/hadoop/spark-3.2.3/examples/src/main/resources/people.csv"

// 先定义数据的模式，即表由哪些列组成，以及列的数据类型
val schema = "name STRING,age INT,job STRING"

// 用到上一行定义的模式，另外设置表头，以及分隔符号为英文逗号，设置完毕后加载文件
val df = spark.read.schema(schema).option("header","true").
    option("sep",";").csv(filePath)

df.show
df.printSchema

// 将表数据写入csv文件中
df.write.csv("file:///home/hadoop/exp14/task3")


/*********************************/


val filePath = "file:///home/hadoop/hadoop/spark-3.2.3/examples/src/main/resources/people.txt"

// 文本类型里面没有模式，直接载入就可以了
val df = spark.read.text(filePath)

df.show
df.printSchema

// 将表数据写入文本文件中
df.write.text("file:///home/hadoop/exp14/task4")


/*********************************/

// 将几行数据存储在列表中
var lst = List(
    ("z3","男","18"),
    ("z4","女","19"),
    ("z5","未知","未知")
)

//加载列表得到DataFrame，然后加上每一列的列名称
val df = spark.createDataFrame(lst).toDF("姓名","性别","年龄")
df.show
df.printSchema