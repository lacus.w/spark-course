val filePath = "file:///home/hadoop/hadoop/spark-3.2.3/examples/src/main/resources/people.json"

// 读json数据源到DateFrame
val df = spark.read.json(filePath)
// 查看df
df.show
df.printSchema

// select查询
df.select(df("name"),df("age")+1).show
df.select(df("name").as("username"),df("age")+1).show

// filter过滤
df.filter(df("age")>20).show

// groupby分组
df.groupBy("age").count.show

// sort排序
df.sort(df("age").desc).show
df.sort(df("age").desc, df("name").asc).show

/********************************/

// withColumn：在表上增加列
import org.apache.spark.sql.functions.expr
val df2 = df.withColumn( "IfWithAge",
            expr("case when age is null then 'NO' else 'YES' end"))
df2.show
df2.printSchema

// drop: 删除列
val df3 = df2.drop("IfWithAge")
df2.show
df2.printSchema

// 使用sum、avg、max、min等聚合函数
import org.apache.spark.sql.{functions => F} // 导入的时候包取别名为F
df.select(F.sum("age"), F.avg("age"), F.min("age"), F.max("age")).show


/********************************/

// 先将DataFrame转为临时表，表名为People
df.createTempView("People")

// 执行sql语句的话就不调用df的函数了，
// 直接让spark从已经设置好的表中查询返回df
spark.sql("select * from People").show
spark.sql("select * from People where age > 20").show

// sql语句中使用sql自带函数肯定没有问题
// sql语句用spark提供的函数也是支持的
// 但是sql语句中用自己定义的函数，可以但是必须要先注册
spark.udf.register("toUpperCaseUDF", (column:String) => column.toUpperCase)

// 注册好一个名叫"toUpperCaseUDF"的函数后，使用到这个函数
//（UDF就是用户自定义函数的意思，一般都会这样写方便区分）
spark.sql("select age, toUpperCaseUDF(name) from People").show