# 在线安装jdk
sudo yum install java-11-openjdk-devel
# 挂载共享文件夹，或者远程连接使用scp拷贝压缩档文件到~/hadoop目录下
sudo ls /media/sf_Downloads/
sudo cp /media/sf_Downloads/hadoop-3.2.4.tar.gz ~
sudo cp /media/sf_Downloads/spark-3.2.3-bin-hadoop3.2.tgz ~
mkdir ~/hadoop
sudo cp ~/spark-3.2.3-bin-hadoop3.2.tgz ~/hadoop/
sudo cp ~/hadoop-3.2.4.tar.gz ~/hadoop/
sudo chown hadoop:hadoop ~/hadoop/*
# 解压缩到~/hadoop目录下：
tar -xf ~/hadoop/hadoop-3.2.4.tar.gz  -C ~/hadoop/
tar -xf ~/hadoop/spark-3.2.3-bin-hadoop3.2.tgz -C ~/hadoop/spark-3.2.3 --strip 1
# 设置环境变量
vi ~/.bashrc
############# 加入以下语句 ################
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk
export HADOOP_HOME=~/hadoop/hadoop-3.2.4
export PATH=$PATH:$HADOOP_HOME/bin
export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
export SPARK_HOME=~/hadoop/spark-3.2.3
export PATH=$SPARK_HOME/bin:$PATH
##########################################
source ~/.bashrc